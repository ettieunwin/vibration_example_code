#include <dolfin.h>
#include <pods.h>
#include "../solver/KLPlateSolverCR.h"
#include <iostream>
#include <fstream>

using namespace pods;
using namespace dolfin;
int main(int argc, char* argv[])
{
  // Sets logger levels
  set_pods_log_level(50, "one");
  dolfin::set_log_level(50);

  double tol;
  std::string statistical_programme;

  if (argc !=2)
  {
    tol = 1e-1;
  }
  else
  {
    tol = atof(argv[1]);
   }

  tol *= 1e-2;

  // Sets variables for MC/MLMC
  int n_cells_L0 = 15;
  int n_samples = 10;
  int L = 4;

  // Sets information about random fields
  double seed = 1880;
  int num_ps = 7;
  double mean_density = 2700.0;
  double scaling = 0;//1e-9;

  // Sets eigenvalues to find
  int freq_start = 1;
  int freq_stop = 1001;

  // Sets save information
  std::string save_path = "../data/";
  std::string solver_type = "kl_plate_cr";

  // Generate solver
  std::vector<QOI> QOIholder;
  std::pair<std::vector<double>, std::vector<double>> stats;

  std::shared_ptr<GenericSolver> solver_class;
  solver_class = std::make_shared<KLPlateSolverCR>(n_cells_L0, freq_start,
						   freq_stop, num_ps,
						   mean_density, scaling);

  MC mc = MC(solver_class, save_path, tol);
  QOIholder = mc.routine(seed);

  //for (int j=0; j<QOIholder.size(); j++)
  //{
  //  std::cout << "Expectation of energy " << freq_start+j <<  ": "
  //	      << QOIholder[j].expectation << std::endl;

  //std::cout << "Variance of energy " << freq_start+j <<  ": "
  //	      << QOIholder[j].variance << std::endl;
  //}

  // File to save energies to.
  std::ofstream file;
  file.open("../energies.csv");
  for (int i=0; i < QOIholder.size(); ++i)
    file << i << ", " << QOIholder[i].expectation <<  ", "
	 << QOIholder[i].variance << std::endl;
}
