#include <dolfin.h>
#include <pods.h>
#include "plate_kl_cr.h"
#include "KLPlateSolverCR.h"
#include <fstream>

using namespace dolfin;
using namespace pods;
//-----------------------------------------------------------------------------
KLPlateSolverCR::KLPlateSolverCR(int n_cells_L0, int freq_start, int freq_stop,
				 int num_ps, double mean, double scaling)
  : _n_cells_L0(n_cells_L0), _freq_start(freq_start), _freq_stop(freq_stop),
    _num_ps(num_ps), _mean(mean), _scaling(scaling)
{
  std::vector<double> mesh_dimensions = {1.0, 0.8};
  _mesh_dimensions = mesh_dimensions;
  parameters["mesh_partitioner"] = "ParMETIS";
}
//-----------------------------------------------------------------------------
std::vector<double> KLPlateSolverCR::pods_solve(PodsMesh& pods_mesh,
						double seed,
						bool fine_mesh)
{
  // Chooses the correct mesh from pods_mesh depending
  std::shared_ptr<const dolfin::Mesh> mesh;
  if (fine_mesh == true)
    mesh = pods_mesh.get_fine_mesh();
  else
    mesh = pods_mesh.get_coarse_mesh();

  MPI_Comm mesh_mpi_comm = mesh->mpi_comm();
  int rank;
  MPI_Comm_rank(mesh_mpi_comm, &rank);

  parameters["std_out_all_processes"] = false;

  auto V = std::make_shared<plate_kl_cr::FunctionSpace>(mesh);

  // Dummy rhs
  auto b_rhs = std::make_shared<PETScVector>(mesh_mpi_comm);
  plate_kl_cr::Form_L L(V);
  L.c = std::make_shared<Constant>(0.0);

  // Stiffness matrix
  auto K = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  plate_kl_cr::Form_k k(V, V);
  assemble_system(*K, *b_rhs, k, L, {});

  // Aluminimum
  const double thickness = 0.003;
  const double E = 70.0e9;
  const double nu = 0.32;
  const double rho_mu = _mean;
  const double D = pow(thickness, 3)*E/(12.0*(1.0 - nu*nu));

  // Build mass matrix
  auto M = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  plate_kl_cr::Form_m m(V, V);
  m.rho = std::make_shared<Constant>(rho_mu);
  m.D = std::make_shared<Constant>(D);
  m.thickness = std::make_shared<Constant>(thickness);
  assemble_system(*M, *b_rhs, m, L, {});

  // Create a probability space
  ProbabilitySpace Omega = ProbabilitySpace("uniform", seed);

  // Scaling size of random number to ensure isn't larger than matrix entry.
  double scaling_kl_plate =
    1.0/7.0*0.1*_mean*thickness*_mesh_dimensions[0]*_mesh_dimensions[1];

  // Generating random point sources
  pods::PointSourceGenerator
    psg = pods::PointSourceGenerator(Omega,
				     mesh_mpi_comm,
				     _num_ps,
				     _mean,
				     scaling_kl_plate,
				     _mesh_dimensions);
  std::vector<std::pair<dolfin::Point, double>> sources = psg.sample();

  // Convert random field information to be compatible with pointers
  // in FEniCS
  std::vector<std::pair<const Point*, double>> _sources;
  for (std::size_t i = 0; i < sources.size(); ++i)
    _sources.push_back({&(sources[i].first), sources[i].second});

  PointSource ps = PointSource(V->sub(1), V->sub(1), _sources);
  ps.apply(*M);

  // Eigensolver
  const std::size_t nevs = 20;
  SLEPcEigenSolver esolver(mesh_mpi_comm, K, M);
  esolver.set_options_prefix("plate_");

  PETScOptions::set("plate_st_ksp_type", "preonly");
  PETScOptions::set("plate_st_pc_type", "lu");
  PETScOptions::set("plate_st_pc_factor_mat_solver_package", "mumps");
  PETScOptions::set("plate_eps_gen_hermitian");
  PETScOptions::set("plate_eps_nev", nevs);
  PETScOptions::set("plate_eps_tol", 1e-3);
  PETScOptions::set("plate_eps_target", 1.0);
  PETScOptions::set("plate_st_type", "sinvert");
  PETScOptions::set("plate_eps_max_it", 100);
  PETScOptions::set("plate_st_pc_type", "cholesky");
  PETScOptions::set("plate_eps_smallest_real");

  esolver.solve(nevs);

  // Extract results
  double r, c;
  PETScVector rx(mesh_mpi_comm);
  PETScVector cx(mesh_mpi_comm);

  std::vector<double> lam;
  std::vector<double> phis;
  std::vector<double> nfs;
  Function eigenfunction(V);
  std::vector<Function> eigenfunctions;

  // Find which processor the point is in
  Point p(0.5, 0.35);
  auto collision = mesh->bounding_box_tree()->compute_first_entity_collision(p);
  auto cells = mesh->num_cells();
  double p_val;

  Array<double> vals(2);
  for (std::size_t ev_no=0; ev_no<nevs; ++ev_no)
  {
    esolver.get_eigenpair(r, c, rx, cx, ev_no);
    *eigenfunction.vector() = rx;
    eigenfunctions.push_back(eigenfunction);

    // Evaluate eigenfunction at point if found on processor
    p_val = -1e8;
    if (collision < cells)
    {
      eigenfunction(vals,p);
      p_val = vals[1];
    }

    // Communicate to all processors
    double px;
    MPI_Allreduce(&p_val, &px, 1, MPI_DOUBLE, MPI_MAX, mesh_mpi_comm);

    phis.push_back(px);
    nfs.push_back(sqrt(r));
    lam.push_back(r);
  }

  std::vector<double> energies;
  double energy = 0;
  double a, T;
  double R = 1.0*0.8;

  // File to save energies to.
  //std::ofstream file;
  //file.open("energies.csv");

  for (int f=_freq_start; f<_freq_stop; f++)
  {
    // Compute energy
    energy = 0;
    double omega = f;
    double eta = 0.05;
    for (int i=1; i<nfs.size(); i++)
    {
      a = pow(phis[i], 2)/(4.0*R);
      T = (pow(omega, 2)*a)/(pow(pow(nfs[i], 2) - pow(omega, 2), 2)
			     + pow(eta*nfs[i]*omega, 2));
      //std::cout << nfs[i] << " Energy per mode: " << T << std::endl;
      energy +=T;
    }

    // Dividing to get property rho*phi_i*phi_j = 1
    energy = energy * (1/(thickness*D));

    // Write Energy to file
    //std::cout << "Energy for omega= " << omega << ": " << energy << std::endl;
    //if (rank == 0)
    //  file << omega << ", " << energy << std::endl;

    energies.push_back(energy);
  }
  //std::cout << "Energies: ";
  //for (int i=0; i<energies.size(); ++i)
  //  std::cout << energies[i] << " ";
  //std::cout << std::endl;


  // Checking orthogonality properties of the plate.
  //for (int i=0; i<5; ++i)
  //{
  //  for (int j =0; j<5; ++j)
  //  {
  //    // Check eigenfunctions
  //    plate_kl_cr::Form_eigfunctions eigfuns(mesh);
  //    eigfuns.eig_a = std::make_shared<Function>(eigenfunctions[i]);
  //    eigfuns.eig_b = std::make_shared<Function>(eigenfunctions[j]);
  //    eigfuns.rho = std::make_shared<Constant>(rho_mu);
  //    eigfuns.D
  //	= std::make_shared<Constant>(pow(thickness, 3)*E/(12.0*(1.0 - nu*nu)));
  //    eigfuns.thickness = std::make_shared<Constant>(thickness);
  //    double a_eigfuns = assemble(eigfuns);
  //   std::cout << "Inner product of eigenfunctions " << i << ", "
  //             << j << ": " << a_eigfuns << std::endl;
  //  }
  // }

  return energies;
}
//-----------------------------------------------------------------------------
std::shared_ptr<Mesh> KLPlateSolverCR::_get_initial_mesh(MPI_Comm comm)
{
  return std::make_shared<RectangleMesh>(comm, Point(0., 0.),
                                         Point(_mesh_dimensions[0],
                                               _mesh_dimensions[1]),
                                         _n_cells_L0, _n_cells_L0);
}
//-----------------------------------------------------------------------------
// Generates mesh of a given level
void KLPlateSolverCR::get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
				 int level)
{
  // Creates the other meshes as necessary using dolfin::refine.
  PodsMesh::create_meshes(pods_mesh, _get_initial_mesh(comm),
			  comm, level);

  return;
}
//-----------------------------------------------------------------------------
solver_info KLPlateSolverCR::get_solver_info()
{
  solver_info info;

  std::ostringstream Convert;
  Convert << std::fixed << std::setprecision(10) << _scaling;
  std::string scaling_str = Convert.str();

  info.problem = "kl_plate";
  info.random_field = std::to_string(_num_ps) + " point sources / max magnitude " + scaling_str + "mass";

  std::vector<std::string> freq;
  for (int q=_freq_start; q<_freq_stop; ++q)
    freq.push_back(std::to_string(q));
  info.qoi = freq;

  info.mesh = std::to_string(_n_cells_L0);

  return info;
}
//-----------------------------------------------------------------------------
std::vector<int> KLPlateSolverCR::get_min_num_cores_per_level(int num_levels)
{
  std::vector<int> minCoresAtLevel;
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  for (int i = 4; i<num_levels; i++)
    minCoresAtLevel.push_back(1);

  return minCoresAtLevel;
}
//-----------------------------------------------------------------------------
