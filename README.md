# Vibrations example code #

This repository contains the code for Chapter 4 in HJT Unwin's thesis.
It enables uncertainty to be quantified using both the MLMC and MC methods with the podS library for:

* wave equation
* Kirchhoff-Love plate equation
* three dimensional linear elastodynamics
* energry density in Kichhoff-Love plate.

The meshes used in the three dimensional linear elastoynamics need to be built from within the solver directory for the slender and curved plate examples.