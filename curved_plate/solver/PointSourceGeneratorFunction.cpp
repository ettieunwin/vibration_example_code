// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// First added:  2017-03-29
// Last changed: 2017-03-29

#include "PointSourceGeneratorFunction.h"
#include <pods.h>
#include <dolfin.h>

using namespace pods;
//------------------------------------------------------------------------------
std::vector<std::pair<dolfin::Point, double>>
  PointSourceGeneratorFunction::sample()
{
  // Generate random number generator
  std::vector<double> range = {-1, 1};
  std::mt19937 gen;
  std::function<double()> random_number_generator
    = _Omega.generate_random_number_generator(gen, range);
  
  int dim = _mesh_dimensions.size();
  int child_rank;
  MPI_Comm_rank(_child_comm, &child_rank);

  std::vector<std::pair<dolfin::Point, double>> sources;

  if (child_rank == 0)
  {
    for (int i=0; i< _num_ps; ++i)
    {
      dolfin::Array<double> points(dim);
      std::pair<dolfin::Point, double> pair;

      //Randomly locates point - between 0 and maximum mesh dimensions
      for (int i=0; i<dim; ++i)
      {
	if (i<2)
	  points[i] = std::abs(random_number_generator())*_mesh_dimensions[i];
	else
	{
	  double z = 0.01*cos(30.0*M_PI*points[0])*sin(30.0*M_PI*points[1])
	    + 0.5*_mesh_dimensions[2];
	  points[i] = z + random_number_generator()*0.1*_mesh_dimensions[2];
	}
      }
      const dolfin::Point new_point = dolfin::Point(points);
      
      //Randomly sets location of magnitude - scaling*(0, 1)
      double magnitude = _scaling*std::abs(random_number_generator());

      pair = std::make_pair(new_point, magnitude);
      sources.push_back(pair);
    }
  }

  pods_log(20, "generated random point sources");
  return sources;
}
//------------------------------------------------------------------------------
