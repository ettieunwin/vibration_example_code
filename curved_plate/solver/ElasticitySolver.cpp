#include "ElasticVib.h"
#include "ElasticitySolver.h"
#include "PointSourceGeneratorFunction.h"
#include <pods.h>

#include <math.h>

using namespace pods;
using namespace dolfin;
//-----------------------------------------------------------------------------
ElasticitySolver::ElasticitySolver(std::vector<std::string> meshes,
				   int eig_start, int eig_stop,
				   int num_ps, double mean, double scaling)
  : _meshes(meshes), _eig_start(eig_start), _eig_stop(eig_stop),
    _num_ps(num_ps), _mean(mean), _scaling(scaling)
{
  // Maybe can calcualte this later...
  std::vector<double> mesh_dimensions = {0.1, 0.1, 0.01};
  _mesh_dimensions = mesh_dimensions;
  parameters["mesh_partitioner"] = "ParMETIS";
}
//-----------------------------------------------------------------------------
std::vector<double> ElasticitySolver::pods_solve(PodsMesh& pods_mesh,
						 double seed,
						 bool fine_mesh)
{
  // Chooses the correct mesh from pods_mesh depending
  std::shared_ptr<const dolfin::Mesh> mesh;
  if (fine_mesh == true)
    mesh = pods_mesh.get_fine_mesh();
  else
    mesh = pods_mesh.get_coarse_mesh();

  // Gets correct communicator from mesh
  MPI_Comm mesh_mpi_comm = mesh->mpi_comm();

  // Gets function space
  auto V = std::make_shared<ElasticVib::FunctionSpace>(mesh);
  auto u_soln = std::make_shared<Function>(V);

  // Dummy rhs
  auto b_rhs = std::make_shared<PETScVector>(mesh_mpi_comm);
  ElasticVib::Form_L L(V);
  L.c = std::make_shared<Constant>(0.0, 0.0, 0.0);

  // Assemble stiffness matrix
  auto K = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  ElasticVib::Form_k k(V, V);
  const double E = 70e9;
  const double nu = 0.32;
  const double rho_mu = _mean;
  k.mu = std::make_shared<Constant>(E/(2.0*(1.0 + nu)));
  k.lam = std::make_shared<Constant>(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)));
  assemble_system(*K, *b_rhs, k, L, {});

  // Assemble mass matrix
  auto M = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  ElasticVib::Form_m m(V, V);
  m.rho = std::make_shared<Constant>(rho_mu);
  assemble_system(*M, *b_rhs, m, L, {});

  // Create a probability space
  ProbabilitySpace Omega = ProbabilitySpace("uniform", seed);
  pods_log(20, "created probability space");

  // Scaling size of random number to ensure isn't larger than matrix entry.
  double scaling_elasticity
   = _scaling*_mean*_mesh_dimensions[0]*_mesh_dimensions[1]*_mesh_dimensions[2];

  PointSourceGeneratorFunction
    psg = PointSourceGeneratorFunction(Omega,
				       mesh_mpi_comm,
				       _num_ps,
				       _mean,
				       scaling_elasticity,
				       _mesh_dimensions);
  std::vector<std::pair<dolfin::Point, double>> sources = psg.sample();

  // Convert random field information to be compatible with pointers
  // in FEniCS
  std::vector<std::pair<const Point*, double>> _sources;
  for (std::size_t i = 0; i < sources.size(); ++i)
    _sources.push_back({&(sources[i].first), sources[i].second});

  // Apply point sources
  PointSource ps = PointSource(V, V, _sources);
  ps.apply(*M);

  // Sets up eigensolver
  SLEPcEigenSolver esolver(mesh_mpi_comm, K, M);
  
  esolver.parameters["spectrum"] = "smallest real";
  esolver.parameters["problem_type"] = "gen_hermitian";
  esolver.parameters["solver"] = "jacobi-davidson";
  PETScOptions::set("eps_view");
  PETScOptions::set("eps_monitor");
  
  VectorSpaceBasis near_null_space = build_near_nullspace(*V, *u_soln->vector());
  esolver.set_deflation_space(near_null_space);
  K->set_near_nullspace(near_null_space);

  PETScOptions::set("eps_jd_blocksize", 3);
  PETScOptions::set("eps_gd_double_expansion");
  PETScOptions::set("st_type", "precond");
  PETScOptions::set("eps_smallest_real");
  PETScOptions::set("eps_tol", 5e-8);
  PETScOptions::set("st_ksp_type", "cg");
  PETScOptions::set("st_pc_type", "gamg");
  PETScOptions::set("st_pc_gamg_type", "agg");
  PETScOptions::set("st_pc_gamg_agg_nsmooths", 1);
  PETScOptions::set("st_ksp_rtol", 1e-3);
  PETScOptions::set("st_pc_gamg_threshold", 0.02);
  PETScOptions::set("st_pc_gamg_coarse_eq_limit", 1000);
  PETScOptions::set("st_pc_gamg_square_graph", 1);
  PETScOptions::set("st_pc_gamg_reuse_interpolation", true);
  PETScOptions::set("st_mg_levels_esteig_ksp_type", "cg");
  PETScOptions::set("st_mg_levels_ksp_type", "chebyshev");
  PETScOptions::set("st_mg_levels_pc_type", "jacobi"); 

  
  // Solves for firs nevs eigenvalues
  const std::size_t nevs = _eig_stop;
  esolver.solve(nevs);

  // Extract eigenvalues
  double r, c;
  PETScVector rx(mesh_mpi_comm);
  PETScVector cx(mesh_mpi_comm);
  std::vector<double> lam;
  for (std::size_t j=0; j<nevs; ++j)
  {
    esolver.get_eigenpair(r, c, rx, cx, j);
    lam.push_back(r);
  }

  std::vector<double> nf;
  int count = 0;
  for (int i = _eig_start; i < _eig_stop; ++i)
    {
      nf.push_back(lam[i]);
      count += 1;
    }

  return nf;
}
//----------------------------------------------------------------------------
// Generates mesh of a given level
void ElasticitySolver::get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
				  int level)
{
  // Reads in meshes from file
  PodsMesh::read_meshes(pods_mesh, _meshes, comm, level);
  pods_log(20, "read in meshes");

  return;
}
//-----------------------------------------------------------------------------
solver_info ElasticitySolver::get_solver_info()
{
  solver_info info;
  info.problem = "elasticity";
  info.random_field = std::to_string(_num_ps)
    + " point sources / max magnitude " + std::to_string(_scaling) + "mass";

  std::vector<std::string> eigs;
  for (int q=_eig_start; q<_eig_stop; ++q)
    eigs.push_back(std::to_string(q));
  info.qoi = eigs;

  info.mesh = "meshes pre generated";

  return info;
}
//-----------------------------------------------------------------------------
dolfin::VectorSpaceBasis
ElasticitySolver::build_near_nullspace(const dolfin::FunctionSpace& V,
				       const GenericVector& x)
{
  // Get subspaces
  auto V0 = V.sub(0);
  auto V1 = V.sub(1);
  auto V2 = V.sub(2);

  // Create vectors for nullspace basis
  std::vector<std::shared_ptr<dolfin::GenericVector>> basis(6);
  for (std::size_t i = 0; i < basis.size(); ++i)
    basis[i] = x.copy();

  // x0, x1, x2 translations
  V0->dofmap()->set(*basis[0], 1.0);
  V1->dofmap()->set(*basis[1], 1.0);
  V2->dofmap()->set(*basis[2], 1.0);

  // Rotations
  V0->set_x(*basis[3], -1.0, 1);
  V1->set_x(*basis[3],  1.0, 0);

  V0->set_x(*basis[4],  1.0, 2);
  V2->set_x(*basis[4], -1.0, 0);

  V2->set_x(*basis[5],  1.0, 1);
  V1->set_x(*basis[5], -1.0, 2);

  // Apply
  for (std::size_t i = 0; i < basis.size(); ++i)
    basis[i]->apply("add");

  // Create vector space and orthonormalize
  VectorSpaceBasis vector_space(basis);
  vector_space.orthonormalize();
  return vector_space;
}
//-----------------------------------------------------------------------------
