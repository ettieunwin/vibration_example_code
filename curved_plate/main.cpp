#include <dolfin.h>
#include <pods.h>
#include "solver/ElasticitySolver.h"
#include <iostream>
#include <fstream>

using namespace dolfin;
using namespace pods;

int main(int argc, char* argv[])
{
  double tol;
  if (argc != 2)
    {
      tol = 1e-1;
    }
  else
    {
      tol = atof(argv[1]);
    }
  
  // Sets logger levels
  set_pods_log_level(0);
  set_log_level(50);

  // Set variables for MC/MLMC
  int n_samples = 4;
  int L = 4;
  std::string dir = "../solver/meshes/";
  std::vector<std::string> mlmc_meshes
    = {dir+"mesh0.xdmf", dir+"mesh1.xdmf", dir+"mesh2.xdmf",
       dir+"mesh3.xdmf"};

  std::vector<std::string> mc_meshes = {dir+"mesh3.xdmf"};

  tol *= 5.2e8;
  
  // Sets information about random field
  double seed = 201;
  double mean_density = 2700.0;
  int num_ps = 20;
  double scaling = 5e-3;

  // Sets eigenvalues to find
  int eig_start =0;
  int eig_stop = 1;

  // Sets save information
  std::string save_path = "../data/";
  std::string solver_type = "elasticity3D";
  std::string statistical_programme = "mc";

  // Generate solver
  std::vector<QOI> QOIholder;
  
  std::shared_ptr<GenericSolver> solver_class;

  // Runs mc/ mlmc routine
  if (statistical_programme == "mlmc")
  {
    solver_class = std::make_shared<ElasticitySolver>(mlmc_meshes, eig_start,
						      eig_stop, num_ps,
						      mean_density, scaling);
    MLMC mlmc = MLMC(solver_class, save_path, tol, n_samples);
    QOIholder = mlmc.routine(seed);
  }
  else
  {
    solver_class = std::make_shared<ElasticitySolver>(mc_meshes, eig_start,
						      eig_stop, num_ps,
						      mean_density, scaling);
    MC mc = MC(solver_class, save_path, tol);
    QOIholder = mc.routine(seed);
  }

  for (int j=0; j<QOIholder.size(); j++)
  {
  std::cout << "Expectation of natural frequency " << eig_start+j <<  ": "
	    << QOIholder[j].expectation << std::endl;

  std::cout << "Variance of natural frequency " << eig_start+j <<  ": "
	    << QOIholder[j].variance << std::endl;
  }
}
