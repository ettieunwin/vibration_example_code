// Copyright (C) 2017 Ettie Unwin
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-06
// Last changed: 2017-04-06

#include "../solver/WaveSolver.h"
#include <pods.h>
#include <dolfin.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace pods;

int main(int argc, char* argv[])
{
  // Sets optional command line options
  double tol;
  int n_cells_L0;
  std::string statistical_programme;

  if (argc != 3)
  {
    tol = 1e-2;
    n_cells_L0 =  30;
  }
  else
  {
    tol = atof(argv[1]);
    n_cells_L0 = atof(argv[2]);
  }

  tol*= 3.68;

  // Sets logger levels
  set_pods_log_level(0, "one");
  dolfin::set_log_level(50);

  // Sets variables for MC/MLMC
  int n_samples = 10;
  int L = 4;

  // Sets information about random fields
  double seed = 1010.0;
  int num_ps = 30;
  double mean_density = 8000.0;
  double scaling = 0.1;

  // Sets eigenvalues to find.  First ones that don't go negative
  // with point sources
  int eig_start = 6;
  int eig_stop = 7;

  // Sets save information
  std::string save_path = "../data/";
  std::string solver_type = "wave";

  // Generate solver
  std::vector<QOI> QOIholder;
  std::pair<std::vector<double>, std::vector<double>> stats;
  std::shared_ptr<GenericSolver> solver_class;

  solver_class = std::make_shared<WaveSolver>(n_cells_L0, eig_start,
					      eig_stop, num_ps,
					      mean_density, scaling);


  MLMC mlmc = MLMC(solver_class, save_path, tol, n_samples, L, 12);
  QOIholder = mlmc.routine(seed);

  // Prints quantities of interest to std::cout
  for (std::size_t j=0; j<QOIholder.size(); ++j)
    {
    std::cout << "Expectation of natural frequency " << eig_start+j <<  ": "
	      << QOIholder[j].expectation << std::endl;

    std::cout << "Variance of natural frequency " << eig_start+j <<  ": "
	      << QOIholder[j].variance << std::endl;
  }
}
