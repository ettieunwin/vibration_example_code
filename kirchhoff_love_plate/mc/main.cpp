#include <dolfin.h>
#include <pods.h>
#include "../solver/KLPlateSolverCR.h"
#include <iostream>
#include <fstream>

using namespace pods;
using namespace dolfin;
int main(int argc, char* argv[])
{
  // Sets logger levels
  set_pods_log_level(0, "all");
  dolfin::set_log_level(50);

  double tol;
  int n_cells_L0;

  if (argc !=3)
  {
    tol =1e-1;
    n_cells_L0 = 25;
  }
  else
  {
    tol = atof(argv[1]);
    n_cells_L0 = atof(argv[2]);
   }

  tol *= 1.2e6;

  // Sets variables for MC/MLMC
  int n_samples = 10;
  int L = 4;

  // Sets information about random fields
  double seed = 8;
  int num_ps = 30;
  double mean_density = 2700.0;
  double scaling = 0.01;

  // Sets eigenvalues to find
  int eig_start = 2;
  int eig_stop = 3;

  // Sets save information
  std::string save_path = "../data/";
  std::string solver_type = "kl_plate_cr";

  // Generate solver
  std::vector<QOI> QOIholder;
  std::pair<std::vector<double>, std::vector<double>> stats;

  std::shared_ptr<GenericSolver> solver_class;
  solver_class = std::make_shared<KLPlateSolverCR>(n_cells_L0, eig_start,
						   eig_stop, num_ps,
						   mean_density, scaling);


  MC mc = MC(solver_class, save_path, tol);
  QOIholder = mc.routine(seed);


  for (int j=0; j<QOIholder.size(); j++)
  {
    std::cout << "Expectation of natural frequency " << eig_start+j <<  ": "
	      << QOIholder[j].expectation << std::endl;

    std::cout << "Variance of natural frequency " << eig_start+j <<  ": "
	      << QOIholder[j].variance << std::endl;
  }
}
