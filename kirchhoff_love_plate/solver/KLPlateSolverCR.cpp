#include <dolfin.h>
#include <pods.h>
#include "plate_kl_cr.h"
#include "KLPlateSolverCR.h"

using namespace dolfin;
using namespace pods;
//-----------------------------------------------------------------------------
KLPlateSolverCR::KLPlateSolverCR(int n_cells_L0, int eig_start, int eig_stop,
				 int num_ps, double mean, double scaling)
  : _n_cells_L0(n_cells_L0), _eig_start(eig_start), _eig_stop(eig_stop),
    _num_ps(num_ps), _mean(mean), _scaling(scaling)
{
  std::vector<double> mesh_dimensions = {0.1, 0.13};
  _mesh_dimensions = mesh_dimensions;
  parameters["mesh_partitioner"] = "ParMETIS";
}
//-----------------------------------------------------------------------------
std::vector<double> KLPlateSolverCR::pods_solve(PodsMesh& pods_mesh,
						double seed,
						bool fine_mesh)
{
  // Chooses the correct mesh from pods_mesh depending
  std::shared_ptr<const dolfin::Mesh> mesh;
  if (fine_mesh == true)
    mesh = pods_mesh.get_fine_mesh();
  else
    mesh = pods_mesh.get_coarse_mesh();

  MPI_Comm mesh_mpi_comm = mesh->mpi_comm();

  parameters["std_out_all_processes"] = false;

  auto V = std::make_shared<plate_kl_cr::FunctionSpace>(mesh);

  // Dummy rhs
  auto b_rhs = std::make_shared<PETScVector>(mesh_mpi_comm);
  plate_kl_cr::Form_L L(V);
  L.c = std::make_shared<Constant>(0.0);

  // Stiffness matrix
  auto K = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  plate_kl_cr::Form_k k(V, V);
  assemble_system(*K, *b_rhs, k, L, {});

  // Aluminimum
  const double thickness = 0.001;
  const double E = 70.0e9;
  const double nu = 0.32;
  const double rho_mu = _mean;

  // Build mass matrix
  auto M = std::make_shared<PETScMatrix>(mesh_mpi_comm);
  plate_kl_cr::Form_m m(V, V);
  m.rho = std::make_shared<Constant>(rho_mu);
  m.D = std::make_shared<Constant>(pow(thickness, 3)*E/(12.0*(1.0 - nu*nu)));
  m.thickness = std::make_shared<Constant>(thickness);
  assemble_system(*M, *b_rhs, m, L, {});

  // Create a probability space
  ProbabilitySpace Omega = ProbabilitySpace("uniform", seed);

  // Scaling size of random number to ensure isn't larger than matrix entry.
  double scaling_kl_plate =
    _scaling*_mean*thickness*_mesh_dimensions[0]*_mesh_dimensions[1];

  // Generating random point sources
  pods::PointSourceGenerator
    psg = pods::PointSourceGenerator(Omega,
				     mesh_mpi_comm,
				     _num_ps,
				     _mean,
				     scaling_kl_plate,
				     _mesh_dimensions);
  std::vector<std::pair<dolfin::Point, double>> sources = psg.sample();

  // Convert random field information to be compatible with pointers
  // in FEniCS
  std::vector<std::pair<const Point*, double>> _sources;
  for (std::size_t i = 0; i < sources.size(); ++i)
    _sources.push_back({&(sources[i].first), sources[i].second});

  PointSource ps = PointSource(V->sub(1), V->sub(1), _sources);
  ps.apply(*M);

  // Eigensolver
  const std::size_t nevs = _eig_stop;
  SLEPcEigenSolver esolver(mesh_mpi_comm, K, M);
  esolver.set_options_prefix("plate_");

  PETScOptions::set("plate_st_ksp_type", "preonly");
  PETScOptions::set("plate_st_pc_type", "lu");
  PETScOptions::set("plate_st_pc_factor_mat_solver_package", "mumps");
  PETScOptions::set("plate_eps_gen_hermitian");
  PETScOptions::set("plate_eps_nev", nevs);
  PETScOptions::set("plate_eps_tol", 1e-3);
  PETScOptions::set("plate_eps_target", 1.0);
  PETScOptions::set("plate_st_type", "sinvert");
  PETScOptions::set("plate_eps_max_it", 100);
  PETScOptions::set("plate_st_pc_type", "cholesky");
  PETScOptions::set("plate_eps_smallest_real");

  esolver.solve(nevs);

  // Extract results
  double r, c;
  PETScVector rx(mesh_mpi_comm);
  PETScVector cx(mesh_mpi_comm);
  std::vector<double> lam;

  for (std::size_t ev_no=0; ev_no<nevs; ++ev_no)
  {
    esolver.get_eigenpair(r, c, rx, cx, ev_no);
    lam.push_back(r);
  }

  int count = 0;
  std::vector<double> nf;
  for (int i = _eig_start; i < _eig_stop; ++i)
  {
      nf.push_back(lam[i]);
      count += 1;
  }

  std::string msg = "eigvs: ";
  for (int i=0; i<nf.size(); i++)
    msg += std::to_string(nf[i]) + " ";
  pods_log(20, msg);
  return nf;
}
//-----------------------------------------------------------------------------
std::shared_ptr<Mesh> KLPlateSolverCR::_get_initial_mesh(MPI_Comm comm)
{
  return std::make_shared<RectangleMesh>(comm, Point(0., 0.),
                                         Point(_mesh_dimensions[0],
                                               _mesh_dimensions[1]),
                                         _n_cells_L0, _n_cells_L0);
}
//-----------------------------------------------------------------------------
// Generates mesh of a given level
void KLPlateSolverCR::get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
				 int level)
{
  // Creates the other meshes as necessary using dolfin::refine.
  PodsMesh::create_meshes(pods_mesh, _get_initial_mesh(comm),
			  comm, level);

  return;
}
//-----------------------------------------------------------------------------
solver_info KLPlateSolverCR::get_solver_info()
{
  solver_info info;

  std::ostringstream Convert;
  Convert << std::fixed << std::setprecision(10) << _scaling;
  std::string scaling_str = Convert.str();

  info.problem = "kl_plate";
  info.random_field = std::to_string(_num_ps) + " point sources / max magnitude " + scaling_str + "mass";

  std::vector<std::string> eigs;
  for (int q=_eig_start; q<_eig_stop; ++q)
    eigs.push_back(std::to_string(q));
  info.qoi = eigs;

  info.mesh = std::to_string(_n_cells_L0);

  return info;
}
//-----------------------------------------------------------------------------
std::vector<int> KLPlateSolverCR::get_min_num_cores_per_level(int num_levels)
{
  std::vector<int> minCoresAtLevel;
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  minCoresAtLevel.push_back(1);
  for (int i = 4; i<num_levels; i++)
    minCoresAtLevel.push_back(1);

  return minCoresAtLevel;
}
//-----------------------------------------------------------------------------
