#ifndef KLPLATESOLVERCR_H
#define KLPLATESOLVERCR_H

#include <pods.h>
#include <dolfin.h>

using namespace pods;

class PointSourceGenerator;

class KLPlateSolverCR: public GenericSolver
{
public:
  // Constructor
  KLPlateSolverCR(int n_cells_L0, int eig_start, int eig_stop,
		  int num_ps,
		  double mean, double scaling);

  // Solves the wave equation eigenvalue problem
  std::vector<double> pods_solve(PodsMesh& pods_mesh,
				 double seed,
				 bool fine_mesh) override;

  // Generates meshes
  void get_meshes(MPI_Comm comm, PodsMesh& pods_mesh,
		  int level) override;

  // Returns number of qois
  int get_num_qoi() override
  {
    return _eig_stop-_eig_start;
  }

  // Returns solver info
  solver_info get_solver_info() override;

  // Returns minimum number of cores required for each level
  std::vector<int> get_min_num_cores_per_level(int num_levels) override;

private:

  // Generates initial mesh
  std::shared_ptr<dolfin::Mesh> _get_initial_mesh(MPI_Comm comm);

  // Mesh dimensions
  std::vector<double> _mesh_dimensions;

  // Number of cells in first level of mesh
  int _n_cells_L0;

  // First eigen value
  int _eig_start;

  // Last eigen value
  int _eig_stop;

  // Number of point sources
  int _num_ps;

  // Mean
  double _mean;

  // Scaling
  double _scaling;

};

#endif
