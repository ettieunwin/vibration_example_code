#include <dolfin.h>
#include <pods.h>
#include <iostream>
#include <fstream>
#include "../solver/ElasticVib.h"

using namespace dolfin;
using namespace pods;

int main()
{
  std::string save_path = "../meshes";

  for (int i=0; i<5; ++i)
  {
    // Set variables for MC/MLMC
    int n_cells_L0 = 7*pow(2,i);
    std::cout << "Number of cells in each direction:" <<  n_cells_L0 << std::endl;
    std::vector<double> mesh_dimensions = {0.005, 0.1, 0.1};

    Mesh mesh = BoxMesh(Point(0.0, 0.0, 0.0),
			Point(mesh_dimensions[0],
			      mesh_dimensions[1],
			      mesh_dimensions[2]),
			n_cells_L0/5,
			n_cells_L0,
			n_cells_L0);
    std::vector<double>& coords = mesh.coordinates();

    XDMFFile xdmf(save_path+ "/mesh"+ std::to_string(i)+".xdmf");
    xdmf.write(mesh);

    auto V =
      std::make_shared<ElasticVib::FunctionSpace>(std::make_shared<Mesh>(mesh));
    std::cout << mesh.num_cells() << " " << mesh.hmax() << " " << V->dim() << std::endl;
  }
}
